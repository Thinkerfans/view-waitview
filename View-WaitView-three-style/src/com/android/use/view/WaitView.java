package com.android.use.view;



import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.user.view.R;

/**
 * 类说明：
 * 
 * @author xinhui.cheng
 * @date 2014-1-3
 * @version 1.0
 */
public class WaitView extends LinearLayout {

	private ProgressBar mWaitPgb;
	private TextView mWaitTv;
	private ImageView mEmptyHintIv;
	private Button mBtn;
	private WaitViewListener mListener;

	public interface WaitViewListener {
		void onClickForRetry(View v);
	}

	public WaitView(Context context) {
		super(context);
		init();
	}

	public WaitView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	private void init() {
		View view = LayoutInflater.from(getContext()).inflate(
				R.layout.wait_view_layout, null);
		view.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));
		addView(view);
		mWaitPgb = (ProgressBar) view
				.findViewById(R.id.wait_view_layout_progress_bar);
		mWaitTv = (TextView) view.findViewById(R.id.wait_view_layout_textview);
		mEmptyHintIv = (ImageView) view.findViewById(R.id.wait_view_hint_iv);
		mBtn = (Button) view.findViewById(R.id.wait_view_layout_btn);
		mBtn.setVisibility(View.GONE);
		this.setVisibility(View.VISIBLE);
	}

	public void setOnWaitViewListener(WaitViewListener l) {
		mListener = l;
	}

	public void showErrorView(int hintResId) {
		mEmptyHintIv.setVisibility(View.GONE);
		mWaitPgb.setVisibility(View.GONE);
		mWaitTv.setVisibility(View.VISIBLE);
		mWaitTv.setText(hintResId);
		setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mListener == null) {
					return;
				}
				mWaitPgb.setVisibility(View.VISIBLE);
				mWaitTv.setText(R.string.loading_wait);
				mListener.onClickForRetry(v);
				setOnClickListener(null);
			}
		});
		setVisibility(View.VISIBLE);
	}

	public void showErrorView() {
		showErrorView(R.string.loaded_failed_please_retry);
	}

	/**
	 * @param v
	 *            需要设置不显示的view
	 */
	public void showErrorView(View v) {
		showErrorView(R.string.loaded_failed_please_retry);
		v.setVisibility(View.GONE);
	}

	/**
	 * @param v
	 *            需要设置不显示的view
	 */
	public void showErrorView(View v, int hintResId) {
		showErrorView(hintResId);
		v.setVisibility(View.GONE);
	}

	public void showEmptyView(int hintResId) {
		mEmptyHintIv.setVisibility(View.VISIBLE);
		mWaitPgb.setVisibility(View.GONE);
		mWaitTv.setVisibility(View.VISIBLE);
		mWaitTv.setText(hintResId);
		setVisibility(View.VISIBLE);
	}

	public void showEmptyView(CharSequence hintString) {
		mEmptyHintIv.setVisibility(View.VISIBLE);
		mWaitPgb.setVisibility(View.GONE);
		mWaitTv.setVisibility(View.VISIBLE);
		mWaitTv.setText(hintString);
		setVisibility(View.VISIBLE);
	}

	/**
	 * @param v
	 *            需要设置不显示的view
	 */
	public void showEmptyView(View v, int hintResId) {
		showEmptyView(hintResId);
		v.setVisibility(View.GONE);
	}

	public void showEmptyView(View v, CharSequence hintString) {
		showEmptyView(hintString);
		v.setVisibility(View.GONE);
	}

	public void showWaitView() {
		mEmptyHintIv.setVisibility(View.GONE);
		mWaitPgb.setVisibility(View.VISIBLE);
		mWaitTv.setVisibility(View.VISIBLE);
		setVisibility(View.VISIBLE);
		mWaitTv.setText(R.string.loading_wait);
	}

	/**
	 * 
	 * @param v
	 *            需要设置不显示的view
	 */
	public void showWaitView(View v) {
		showWaitView();
		v.setVisibility(View.GONE);
	}
	
	public void hideButton(){
		mBtn.setVisibility(View.GONE);
	}
	
	public void showButton(String text , OnClickListener l){
		mBtn.setText(text);
		mBtn.setOnClickListener(l);
		mBtn.setVisibility(View.VISIBLE);
	}
	
	public void showButton(int resId , OnClickListener l){
		mBtn.setText(resId);
		mBtn.setOnClickListener(l);
		mBtn.setVisibility(View.VISIBLE);
	}

}
