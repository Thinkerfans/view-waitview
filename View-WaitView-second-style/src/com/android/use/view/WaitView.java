package com.android.use.view;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

/**
 * 类说明：
 * @author thinker.fans
 * @date 2014-4-25
 * @version 1.0
 */
public class WaitView extends LinearLayout {

	private ProgressBar mWaitPgb;
	private TextView mWaitTv;
	private WaitViewListener mListener;

	public interface WaitViewListener {
		void onClickForRetry(View v);
	}

	public WaitView(Context context) {
		super(context);
		init();
	}

	public WaitView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	private void init() {
		View view = LayoutInflater.from(getContext()).inflate(R.layout.wait_view_layout, null);
		view.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));
		addView(view);
		mWaitPgb = (ProgressBar) findViewById(R.id.wait_progressbar);
		mWaitTv = (TextView) findViewById(R.id.wait_text);
		
	}

	private View makeProgressView() {
		mWaitPgb = new ProgressBar(getContext());
		// mWaitPgb.setScrollBarStyle(android.R.style.Widget_ProgressBar_Small_Inverse);
		mWaitPgb.setScrollBarStyle(android.R.style.Widget_ProgressBar_Small);
		// mWaitPgb.setScrollBarStyle(android.R.attr.progressBarStyleSmall);
		LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		mWaitPgb.setLayoutParams(lp);
		return mWaitPgb;
	}

	private View makeWaitTv() {
		mWaitTv = new TextView(getContext());
		LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		lp.leftMargin = 10;
		mWaitTv.setLayoutParams(lp);
		mWaitTv.setTextColor(Color.WHITE);
		return mWaitTv;
	}

	public void setOnWaitViewListener(WaitViewListener l) {
		mListener = l;
	}

	public void showTextView(boolean isShow) {
		if (isShow) {
			mWaitTv.setVisibility(View.VISIBLE);
		} else {
			mWaitTv.setVisibility(View.GONE);
		}
	}

	public void showProgressView(boolean isShow) {
		if (isShow) {
			mWaitPgb.setVisibility(View.VISIBLE);
		} else {
			mWaitPgb.setVisibility(View.GONE);
		}
	}

	public void show(boolean isShow) {
		if (isShow) {
			mWaitPgb.setVisibility(View.VISIBLE);
			mWaitTv.setVisibility(View.VISIBLE);
			setVisibility(View.VISIBLE);
		} else {
			setVisibility(View.GONE);
		}
	}

	public void setWaitingText(int resId) {
		if (resId == 0) {
			throw new IllegalArgumentException("res id can not be zero");
		}
		mWaitTv.setText(resId);
	}

	public void setWaitingText(String text) {
		if (TextUtils.isEmpty(text)) {
			throw new IllegalArgumentException("text can not be null ");
		}
		mWaitTv.setText(text);
	}

	public void showWaitFailView(int hintResId) {
		setWaitingText(hintResId);
		mWaitPgb.setVisibility(View.GONE);
		mWaitTv.setVisibility(View.VISIBLE);

		setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mListener == null) {
					return;
				}
				mWaitPgb.setVisibility(View.VISIBLE);
				mListener.onClickForRetry(v);
				setOnClickListener(null);
			}
		});
	}

	public void showWaitFailView(String tip) {
		setWaitingText(tip);
		
		mWaitPgb.setVisibility(View.GONE);
		mWaitTv.setVisibility(View.VISIBLE);
		setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (mListener == null) {
					return;
				}
				mWaitPgb.setVisibility(View.VISIBLE);
				mListener.onClickForRetry(v);
				setOnClickListener(null);
			}
		});
		setVisibility(View.VISIBLE);
	}
}
