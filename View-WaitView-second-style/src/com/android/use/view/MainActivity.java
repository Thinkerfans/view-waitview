package com.android.use.view;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.android.use.view.WaitView.WaitViewListener;

public class MainActivity extends Activity {

	private WaitView waitView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);	
		
		initWaitView();	
	}
	
	private void initWaitView(){
		
		waitView = (WaitView) findViewById(R.id.waitview);
		waitView.setWaitingText(R.string.loading_wait);
		waitView.show(true);
		waitView.setOnWaitViewListener(new WaitViewListener() {
			public void onClickForRetry(View v) {
				waitView.setWaitingText(R.string.loading_wait);
				waitView.show(true);
			}
		});	
	}

	public void onClick(View v) {
		waitView.showWaitFailView(R.string.loaded_failed_please_retry);
	}
}
